package com.example.getinfomation;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

public class getTime extends Activity {
	
	private int mHour,mMin;
	private Button mButtonBtime,mButtonLtime,mButtonDtime,mButtonFinish;
	static final int mB_TIMEPICKER = 0;
	static final int mL_TIMEPICKER = 1;
	static final int mD_TIMEPICKER = 2;
	
	
	int mLevel,mAge;
	String mName,mGender,mWorkPower,mUid,mToken;
	String mBtime,mLtime,mDtime;
	double mHeight,mWeight;
	int mGenderCode;
	//User Information
	
	
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gettime);
		iniView();
		setListener();
		bundle();
		makeGenderCode();
		//Toast.makeText(getTime.this,""+mToken,Toast.LENGTH_LONG).show();
		if(mLevel==0){
			mWorkPower="Weak";
		}
		else if(mLevel==1){
			mWorkPower="Medium";
			
		}
		else{
			mWorkPower="Strong";
		}
		
	}

	private void makeGenderCode() {
		// TODO Auto-generated method stub
		if(mGender.equals("male")){
			mGenderCode=0;
		}
		else if(mGender.equals("female")){
			mGenderCode=1;
		}
	}

	private void iniView() {
		mButtonBtime=(Button)findViewById(R.id.btn_btime);
		mButtonLtime=(Button)findViewById(R.id.btn_ltime);
		mButtonDtime=(Button)findViewById(R.id.btn_dtime);
		mButtonFinish=(Button)findViewById(R.id.btn_next3);
	}

	private void setListener() {
		mButtonBtime.setOnClickListener(btimepick);
		mButtonLtime.setOnClickListener(ltimepick);
		mButtonDtime.setOnClickListener(dtimepick);
		mButtonFinish.setOnClickListener(finish);
	}

	private void bundle() {
		Bundle bundle=this.getIntent().getExtras();
		mName=bundle.getString("key_Name");
		mUid=bundle.getString("key_Uid");
		mToken=bundle.getString("key_token");
		mGender=bundle.getString("key_Gender");
		mLevel=bundle.getInt("key_Level");
		mAge=bundle.getInt("key_Age");
		mHeight=bundle.getDouble("key_Height");
		mWeight=bundle.getDouble("key_Weight");
	}
	
	private OnClickListener finish =new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(mBtime==null || mLtime==null || mDtime==null )
			{
				Toast.makeText(getTime.this, "Cheak you're notification time", Toast.LENGTH_LONG).show();
			}
			else{
			switch(v.getId()){
			case R.id.btn_next3:
				new AlertDialog.Builder(getTime.this)
				.setTitle("Cheak Information")
				.setIcon(R.drawable.ic_launcher)
				.setMessage("Name:"+mName+
						"\nGender:"+mGender+
						"\nAge:"+mAge+
						"\nHeight:"+mHeight+
						"Cm\nWeight:"+mWeight+
						"Kg\nWorkPower:"+mWorkPower+
						"\nYou're notification time:\n"+mBtime+
						"\t"+mLtime+
						"\t"+mDtime)
				.setPositiveButton("Cancel",new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				})
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						//json && new Activity;
					}
				})
			
				.show();
				break;
			}
			}
		}
	};
	
	private OnClickListener btimepick=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final Calendar c=Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 7);
			c.set(Calendar.MINUTE,0);
			mHour=c.get(Calendar.HOUR_OF_DAY);
			mMin=c.get(Calendar.MINUTE);
			showDialog(mB_TIMEPICKER);
		}
	};
	
	private OnClickListener ltimepick=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final Calendar c=Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 11);
			c.set(Calendar.MINUTE,0);
			mHour=c.get(Calendar.HOUR_OF_DAY);
			mMin=c.get(Calendar.MINUTE);
			showDialog(mL_TIMEPICKER);
		}
	};
	
	private OnClickListener dtimepick=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final Calendar c=Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 18);
			c.set(Calendar.MINUTE,0);
			mHour=c.get(Calendar.HOUR_OF_DAY);
			mMin=c.get(Calendar.MINUTE);
			showDialog(mD_TIMEPICKER);
		}
	};
	
	protected Dialog onCreateDialog(int id){
		switch(id){
		case mB_TIMEPICKER:
		    return new TimePickerDialog(this,
		     B_TimeSetListener,
		      mHour, mMin, false);
		case mL_TIMEPICKER:
		    return new TimePickerDialog(this,
		      L_TimeSetListener,
		      mHour, mMin, false);
		case mD_TIMEPICKER:
		    return new TimePickerDialog(this,
		      D_TimeSetListener,
		      mHour, mMin, false);
		}
		return null;
		
	}
	
	 private TimePickerDialog.OnTimeSetListener B_TimeSetListener
	  = new TimePickerDialog.OnTimeSetListener(){

	   @Override
	   public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	    // TODO Auto-generated method stub
	   mBtime = "" + String.valueOf(hourOfDay) +""
	     + ":" + String.valueOf(minute);
	    mButtonBtime.setText("Breakfast Time:"+mBtime);
	   }
	 };
	 
	 private TimePickerDialog.OnTimeSetListener L_TimeSetListener
	  = new TimePickerDialog.OnTimeSetListener(){

	   @Override
	   public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	    // TODO Auto-generated method stub
	    mLtime = "" + String.valueOf(hourOfDay) +""
	     + ":" + String.valueOf(minute);
	    mButtonLtime.setText("Lunch Time"+mLtime);
	   }
	 };
	 
	 private TimePickerDialog.OnTimeSetListener D_TimeSetListener
	  = new TimePickerDialog.OnTimeSetListener(){

	   @Override
	   public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	    // TODO Auto-generated method stub
	    mDtime = "" + String.valueOf(hourOfDay) +""
	     + ":" + String.valueOf(minute);
	    mButtonDtime.setText("Dinner Time"+mDtime);
	   }
	 };
}
