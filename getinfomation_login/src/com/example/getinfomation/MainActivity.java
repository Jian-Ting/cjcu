package com.example.getinfomation;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Session.StatusCallback;
import com.facebook.model.GraphUser;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {
	String mName,mUid,mToken,mGender;
	private Button mButtonLogin;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mButtonLogin=(Button)findViewById(R.id.bth_login);
		mButtonLogin.setOnClickListener(login);
		//Facebooklogin();
	}
	private void Facebooklogin() {
		// TODO Auto-generated method stub
		Session.openActiveSession(this, true, new StatusCallback() {
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				// TODO Auto-generated method stub
				if (session == Session.getActiveSession()) {
				if(session.isOpened()){
					mToken=session.getAccessToken();
					//Toast.makeText(MainActivity.this, session.getAccessToken(),Toast.LENGTH_LONG).show();
					Request.newMeRequest(session, new GraphUserCallback() {
						
						@Override
						public void onCompleted(GraphUser user, Response response) {
							// TODO Auto-generated method stub
							if(user !=null){
								mName=user.getName();
								mUid=user.getId();
								mGender=user.getProperty("gender").toString();
								//Toast.makeText(MainActivity.this, " "+mName+" "+mUid+" "+mGender, Toast.LENGTH_LONG).show();
								Intent intent =new Intent();
								intent.setClass(MainActivity.this,getOtherinf.class);
								Bundle bundle=new Bundle();
								bundle.putString("key_Name", mName);
								bundle.putString("key_Gender",mGender);
								bundle.putString("key_Uid",mUid);
								bundle.putString("key_token",mToken);
								intent.putExtras(bundle);
								startActivity(intent);
			
							}
						}
					}).executeAsync();
				}
				}
			}
		});
	}
	private OnClickListener login=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Facebooklogin();
	
		}
	};
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}
}
