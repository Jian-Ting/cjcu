package com.example.getinfomation;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;


public class setWorkpower extends Activity {
	private RadioButton mRadioButtonStrong,mRadioButtonMed,mRadioButtonWeak;
	private RadioGroup mRadioGroupWorkpower;
	private Button mButtonNext2;
	int mPowerlevel=2;
	String mName,mGender,mUid,mToken;
	int mAge;
	double mHeight,mWeight;
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.workpower);
		iniView();
		bundle();
		mRadioGroupWorkpower.setOnCheckedChangeListener(workpower);
	}

	private void iniView() {
		mRadioGroupWorkpower=(RadioGroup)findViewById(R.id.radioGroup1);
		mRadioButtonStrong=(RadioButton)findViewById(R.id.radio_strong);
		mRadioButtonMed=(RadioButton)findViewById(R.id.radio_medium);
		mRadioButtonWeak=(RadioButton)findViewById(R.id.radio_weak);
		mButtonNext2=(Button)findViewById(R.id.btn_next2);
		mButtonNext2.setOnClickListener(input2);
	}

	private void bundle() {
		Bundle bundle=this.getIntent().getExtras();
		mName=bundle.getString("key_Name");
		mGender=bundle.getString("key_Gender");
		mToken=bundle.getString("key_token");
		mUid=bundle.getString("key_Uid");
		mAge=bundle.getInt("key_Age");
		mHeight=bundle.getDouble("key_Height");
		mWeight=bundle.getDouble("key_Weight");
	}
	private RadioGroup.OnCheckedChangeListener workpower=new OnCheckedChangeListener() {
		
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			
			if(checkedId==R.id.radio_weak)
				mPowerlevel=0;
			else if(checkedId==R.id.radio_medium)
				mPowerlevel=1;
			else
				mPowerlevel=2;	
		}
	};
	
	private OnClickListener input2=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			//Toast.makeText(setWorkpower.this," "+mToken, Toast.LENGTH_SHORT).show();
			//level upload
			Intent intent=new Intent();
			intent.setClass(setWorkpower.this,getTime.class);
			Bundle bundle=new Bundle();
			bundle.putInt("key_Level", mPowerlevel);
			bundle.putString("key_Name", mName);
			bundle.putString("key_Gender",mGender);
			bundle.putString("key_Uid",mUid);
			bundle.putString("key_token",mToken);
			bundle.putInt("key_Age",mAge);
			bundle.putDouble("key_Height", mHeight);
			bundle.putDouble("key_Weight", mWeight);
			intent.putExtras(bundle); 
			startActivity(intent);
			
		}
	};
}
