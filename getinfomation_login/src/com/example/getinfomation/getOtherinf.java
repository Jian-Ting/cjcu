package com.example.getinfomation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.UserDictionary.Words;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class getOtherinf extends Activity {
	private TextView mTextViewName,mTextViewGender;
	private EditText mEditTextAge,mEditTextHeight,mEditTextWeight;
	private Button mButtonNext;
	private Button mButtonTest;
	String mName,mGender,mUid,mToken;
	int mAge;
	double mHeight,mWeight;
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.otherinf);
		iniViews();
		bundle();
	}

	private void bundle() {
		Bundle bundle=this.getIntent().getExtras();
		mName=bundle.getString("key_Name");
		mGender=bundle.getString("key_Gender");
		mToken=bundle.getString("key_token");
		mUid=bundle.getString("key_Uid");
		mTextViewName.setText("Hello "+mName);
		mTextViewGender.setText("Gender:"+mGender);
		mButtonNext.setOnClickListener(input);
	}
	private OnClickListener input=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			try{
			mAge=Integer.parseInt(mEditTextAge.getText().toString());
			mHeight=Double.parseDouble(mEditTextHeight.getText().toString());
			mWeight=Double.parseDouble(mEditTextWeight.getText().toString());
			Intent intent=new Intent();
			intent.setClass(getOtherinf.this,setWorkpower.class);
			Bundle bundle= new Bundle();
			bundle.putString("key_Name", mName);
			bundle.putString("key_Gender",mGender);
			bundle.putString("key_Uid",mUid);
			bundle.putString("key_token",mToken);
			bundle.putInt("key_Age",mAge);
			bundle.putDouble("key_Height", mHeight);
			bundle.putDouble("key_Weight", mWeight);
			intent.putExtras(bundle); 
			startActivity(intent);
			//Toast.makeText(getOtherinf.this,"ok",Toast.LENGTH_SHORT).show();
			}catch(Exception obj){
				Toast.makeText(getOtherinf.this,"Check you're information",Toast.LENGTH_SHORT).show();
			}
					
			
		}
	};
	
	private void iniViews() {
		mTextViewName=(TextView)findViewById(R.id.Text_name);
		mTextViewGender=(TextView)findViewById(R.id.Text_gender);
		mEditTextAge=(EditText)findViewById(R.id.edit_age);
		mEditTextHeight=(EditText)findViewById(R.id.edit_height);
		mEditTextWeight=(EditText)findViewById(R.id.edit_weight);
		mButtonNext=(Button)findViewById(R.id.btn_next);
	}

}
