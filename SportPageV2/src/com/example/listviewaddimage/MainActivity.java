package com.example.listviewaddimage;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static final int ACTIVITY_REPORY=1000;
	private Button mButtonAdd;
	private ListView mListViewSportDate;
	private TextView mTextViewShowTotalCalorise;
	private ArrayList<String> mArrayListSportName;
	private ArrayList<Integer> mArrayListConsumeCalorise;
	private SportInformationAdapter mAdapter;
	private int mCalorise;
	int mCaloriseTotal;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mTextViewShowTotalCalorise=(TextView)findViewById(R.id.textView_show_caltotal);
		mTextViewShowTotalCalorise.setText("Calories Consume:0");
		mButtonAdd=(Button)findViewById(R.id.button_add_sport);
		mListViewSportDate=(ListView)findViewById(R.id.listView_show_informatiom);
		mArrayListConsumeCalorise=new ArrayList<Integer>();
		mArrayListSportName=new ArrayList<String>();
		mAdapter=new SportInformationAdapter(MainActivity.this,mArrayListSportName,mArrayListConsumeCalorise);
		mListViewSportDate.setAdapter(mAdapter);
		mButtonAdd.setOnClickListener(new OnClickListener() {
	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent();
				intent.setClass(MainActivity.this, ChoseSportMenuActivity.class);
				startActivityForResult(intent, ACTIVITY_REPORY);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if(resultCode==RESULT_OK){
			if(requestCode==ACTIVITY_REPORY){
				Bundle bundle=intent.getExtras();
				mArrayListSportName.add(bundle.getString("SportSpice"));
				mCalorise=bundle.getInt("Calorise");
				mArrayListConsumeCalorise.add(mCalorise);
				mCaloriseTotal+=mCalorise;
				mTextViewShowTotalCalorise.setText("Calories Consume:"+mCaloriseTotal+" Kcal");
				mAdapter.addItem(mAdapter.getCount()+1);
				//Toast.makeText(MainActivity.this,""+mArrayListSportName,Toast.LENGTH_LONG).show();
			}
		}
	};
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
