package com.example.listviewaddimage;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView.FindListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SportInformationAdapter extends BaseAdapter{

	private ArrayList<Integer> mArraylist;
	private TextView mTextViewCalTotal;
	private TextView mTextViewSportspice;
	private TextView mTextViewSportCal;
	private ArrayList<String> mArrayListSportName;
	private ArrayList<Integer> mArrayListConsumeCalorise;
	private ListView mListViewSportInf;
	private Button mButtonDelet;
	private MainActivity mainActivity;
	String mSportSpice, mSportTime;
	Context mContext;
	LayoutInflater inflater;
	public SportInformationAdapter(MainActivity context,ArrayList<String> sportSpiewlist,ArrayList<Integer> callist){
		mArrayListSportName=sportSpiewlist;
		mArrayListConsumeCalorise=callist;
		this.mainActivity=context;
		inflater=LayoutInflater.from(context);
		mArraylist=new ArrayList<Integer>();
		
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mArraylist.size();
		
	}
	public void addItem(int position){
		mArraylist.add(position);
		this.notifyDataSetChanged();
	}
	public void removeItem(int position){
	    if(!mArraylist.isEmpty()){
	    	mArraylist.remove(position);
	        this.notifyDataSetChanged();
	        
	    }
	}
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		convertView=inflater.inflate(R.layout.adapter_sportinformation_row,null);
		mTextViewSportspice=(TextView)convertView.findViewById(R.id.textView_sportinfmation_sportname);
		mTextViewSportCal=(TextView)convertView.findViewById(R.id.textView_sportinfmation_consume);
		mButtonDelet=(Button)convertView.findViewById(R.id.button_sportinfmation_delete);
		mTextViewSportspice.setText(""+mArrayListSportName.get(position));
		mTextViewSportCal.setText(""+mArrayListConsumeCalorise.get(position)+" Kcal");
		mButtonDelet.setOnClickListener(new mButtonDeletitem(this.mainActivity,position));
	
		return convertView;
	}
	class mButtonDeletitem implements OnClickListener{
		private int position;
		private MainActivity mainActivity;
		mButtonDeletitem(MainActivity context, int pos) {
		        this.mainActivity = context;
		        position = pos;
		    }

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			this.mainActivity.mCaloriseTotal-=mArrayListConsumeCalorise.get(position);
			//Toast.makeText(mainActivity, ""+mCalTotal, Toast.LENGTH_LONG).show();
			mArrayListSportName.remove(position);
			mArrayListConsumeCalorise.remove(position);
			mTextViewCalTotal=(TextView)mainActivity.findViewById(R.id.textView_show_caltotal);
			mTextViewCalTotal.setText("Calories Consume:"+this.mainActivity.mCaloriseTotal);
			removeItem(getCount()-1);
			
		}
	}
}
