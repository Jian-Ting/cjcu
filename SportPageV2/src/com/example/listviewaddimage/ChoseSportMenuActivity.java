package com.example.listviewaddimage;

import java.util.ArrayList;
import java.util.zip.Inflater;

import android.R.integer;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;


public class ChoseSportMenuActivity extends Activity {

	private ListView mListViewSportSpice;
	private ArrayList<String> mArrayListSportName;
	private ArrayList<Integer> mArrayListConsumeCal;
	private ArrayList<Integer> mArrayListSportIcon;
	private TextView mTextViewDialogShowCount;
	String[] sportname={"Bicycling","Siwm","FootBall","Run"};
	int[] consumecal={180,264,420,560};
	
	
	private SportSpiceAdapter mAdapter;
	
	private int mCount=0;
	private int mCalorise;
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chose_sportmenu);
		mArrayListProcess();
		mListViewSportSpice=(ListView)findViewById(R.id.listView_SportSpice);
		//Adapter//
		mAdapter=new SportSpiceAdapter(ChoseSportMenuActivity.this,mArrayListSportName,
		mArrayListConsumeCal);
		mListViewSportSpice.setAdapter(mAdapter);
		//Adapter//
		
		mListViewSportSpice.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					final int position, long id) {	
				// TODO Auto-generated method stub
				//Toast.makeText(ChoseSportMenuActivity.this,"Hello"+mArrayListConsumeCal.get(position), Toast.LENGTH_LONG).show();
				AlertDialog.Builder dialog = new AlertDialog.Builder(ChoseSportMenuActivity.this);
				dialog.setTitle("Chose SportCount");
				LayoutInflater inflater = ChoseSportMenuActivity.this.getLayoutInflater();
				View layout = inflater.inflate(R.layout.dialog_sportcount, null);
				dialog.setView(layout);
				mTextViewDialogShowCount=(TextView)layout.findViewById(R.id.textView_dialogsportcount_showchose);
				SeekBar seekbarcount=(SeekBar)layout.findViewById(R.id.seekBar_dialogsportcount_chose);
				seekbarcount.setMax(20);
				seekbarcount.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onProgressChanged(SeekBar seekBar, int progress,
							boolean fromUser) {
						// TODO Auto-generated method stub
						
						mTextViewDialogShowCount.setText(+progress+"  hour");
						mCount=progress;
					}
				});		
				dialog.setNegativeButton("Ok", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if(mCount!=0){
						mCalorise=0;
						mCalorise+=mArrayListConsumeCal.get(position)*mCount;
						//return mCalorise,SportSpice
						Bundle bundle=new Bundle();
						bundle.putInt("Calorise", mCalorise);
						bundle.putString("SportSpice",mArrayListSportName.get(position) );
						Intent intent =new Intent();
						intent.putExtras(bundle);
						setResult(RESULT_OK,intent);
						ChoseSportMenuActivity.this.finish();
						//Toast.makeText(ChoseSportMenuActivity.this,""+mCalorise, Toast.LENGTH_LONG).show();
						}
					}
				});
				dialog.show();
				}
		});
		
		
	}

	private void mArrayListProcess() {
		mArrayListSportName=new ArrayList<String>();
		for(int i=0;i<sportname.length;i++){
		mArrayListSportName.add(sportname[i]);}
		
		mArrayListConsumeCal=new ArrayList<Integer>();
		for(int i=0;i<consumecal.length;i++){
			mArrayListConsumeCal.add(consumecal[i]);}
		
	}
}
