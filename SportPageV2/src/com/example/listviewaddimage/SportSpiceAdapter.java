package com.example.listviewaddimage;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView.FindListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SportSpiceAdapter extends BaseAdapter{

	private ArrayList<Integer> mArraylist;
	private ImageView mImageViewSportIcon;
	private TextView mTextViewSportspice;
	private TextView mTextViewConsumeCal;
	private ArrayList<String> mSportname;
	private ArrayList<Integer> mConsumeCal;
	
	private ChoseSportMenuActivity  chosesportmenuactivity;
	Context mContext;
	LayoutInflater inflater;
	
	private int[] mSportIcon={R.drawable.bicycling_icon,R.drawable.swim_icon,R.drawable.football_icon,R.drawable.running_icon};
	public SportSpiceAdapter(ChoseSportMenuActivity context,ArrayList<String> sportSpiewlist,ArrayList<Integer> concallist){
		mSportname=sportSpiewlist;
		mConsumeCal=concallist;
		this.chosesportmenuactivity=context;
		inflater=LayoutInflater.from(context);
		//mArraylist=new ArrayList<Integer>();
		
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mSportname.size();
		
	}
	public void addItem(int position){
		mArraylist.add(position);
		this.notifyDataSetChanged();
	}
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		convertView=inflater.inflate(R.layout.adapter_sportspice_row, null);
		mTextViewConsumeCal=(TextView)convertView.findViewById(R.id.textView_sportspice_row_Consume);
		mTextViewSportspice=(TextView)convertView.findViewById(R.id.textView_sportspice_row_Sportname);
		mImageViewSportIcon=(ImageView)convertView.findViewById(R.id.imageView_sportspice_row_Sporticon);
		mTextViewSportspice.setText(""+mSportname.get(position));
		mTextViewConsumeCal.setText(""+mConsumeCal.get(position));
		mImageViewSportIcon.setImageResource(mSportIcon[position]);
		
		return convertView;
	}
}