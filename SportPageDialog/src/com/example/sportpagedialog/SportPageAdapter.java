package com.example.sportpagedialog;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView.FindListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SportPageAdapter extends BaseAdapter{

	private ArrayList<Integer> mArraylist;
	private TextView mTextViewCalTotal;
	private TextView mTextViewSportspice;
	private TextView mTextViewSportCal;
	private ArrayList<String> mItemSp;
	private ArrayList<Integer> mItemCal;
	private ListView mListViewSportInf;
	private Button mButtonDelet;
	private MainActivity mainActivity;
	String mSportSpice, mSportTime;
	Context mContext;
	LayoutInflater inflater;
	public SportPageAdapter(MainActivity context,ArrayList<String> sportSpiewlist,ArrayList<Integer> callist){
		mItemSp=sportSpiewlist;
		mItemCal=callist;
		this.mainActivity=context;
		inflater=LayoutInflater.from(context);
		mArraylist=new ArrayList<Integer>();
		
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mArraylist.size();
		
	}
	public void addItem(int position){
		mArraylist.add(position);
		this.notifyDataSetChanged();
	}
	public void removeItem(int position){
	    if(!mArraylist.isEmpty()){
	    	mArraylist.remove(position);
	        this.notifyDataSetChanged();
	        
	    }
	}
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		convertView=inflater.inflate(R.layout.row,null);
		mTextViewSportspice=(TextView)convertView.findViewById(R.id.textSportSp);
		mTextViewSportCal=(TextView)convertView.findViewById(R.id.textViewCal);
		mButtonDelet=(Button)convertView.findViewById(R.id.buttonDelet);
		mTextViewSportspice.setText(""+mItemSp.get(position));
		mTextViewSportCal.setText(""+mItemCal.get(position));
		mButtonDelet.setOnClickListener(new mButtonDeletitem(this.mainActivity,position));
	
		return convertView;
	}
	class mButtonDeletitem implements OnClickListener{
		private int position;
		private MainActivity mainActivity;
		mButtonDeletitem(MainActivity context, int pos) {
		        this.mainActivity = context;
		        position = pos;
		    }

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			this.mainActivity.mCalTotal-=mItemCal.get(position);
			//Toast.makeText(mainActivity, ""+mCalTotal, Toast.LENGTH_LONG).show();
			mItemCal.remove(position);
			mItemSp.remove(position);
			mTextViewCalTotal=(TextView)mainActivity.findViewById(R.id.TextViewCalTotal);
			mTextViewCalTotal.setText("Calories Consume:"+this.mainActivity.mCalTotal);
			removeItem(getCount()-1);
			
		}
	}
}
