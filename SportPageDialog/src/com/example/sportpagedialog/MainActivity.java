package com.example.sportpagedialog;



import java.util.ArrayList;


import android.R.integer;
import android.os.Bundle;
import android.animation.LayoutTransition;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	int mCal, mTime,mCalTotal;
	String mSportSpice, mSportTime;
	boolean mSportSpiceflag = false;
	boolean mSportTimeflag = false;
	private Button mButtonDelet;
	private SportPageAdapter myAdapter;
	private TextView mTextViewCalTotal;
	private TextView mTextViewSportspice;
	private TextView mTextViewSportCal;
	private ListView mListViewSportInf;
	private ArrayList<String> mItemSp;
	private ArrayList<Integer> mItemCal;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button mButtonAdd = (Button) findViewById(R.id.buttonAdd);
		mTextViewCalTotal=(TextView)findViewById(R.id.TextViewCalTotal);
		mListViewSportInf=(ListView)findViewById(R.id.listViewspinf);
		mItemCal=new ArrayList<Integer>();
		mItemSp=new ArrayList<String>();
		myAdapter=new SportPageAdapter(MainActivity.this, mItemSp,mItemCal);
		mListViewSportInf.setAdapter(myAdapter);
		mButtonAdd.setOnClickListener(new OnClickListener() {
				
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				OpenAlertDialog();
			}
		});
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	protected void OpenAlertDialog() {
		// TODO Auto-generated method stub

		AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
		dialog.setTitle("Sport");
		LayoutInflater inflater = MainActivity.this.getLayoutInflater();
		View layoyt = inflater.inflate(R.layout.dialog_sportmenu, null);
		dialog.setView(layoyt);
		dialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if (mSportSpiceflag != false && mSportTimeflag != false) {
					/*Toast.makeText(MainActivity.this, "" +mSportSpice +mSportTime,
							Toast.LENGTH_LONG).show();*/
					mSportSpiceflag=false;
					mSportTimeflag =false;
					mCalTotal+=mCal*mTime;
					mItemSp.add(mSportSpice);
					mItemCal.add(mCal*mTime);
					myAdapter.addItem(myAdapter.getCount()+1);
					mTextViewCalTotal.setText("Calories Consume:"+mCalTotal);
				}
			}
		});

		Spinner mSpinnerSpotMenu = (Spinner) layoyt
				.findViewById(R.id.spinnerSportSp);
		ArrayAdapter adapterSportSpeice = ArrayAdapter.createFromResource(
				MainActivity.this, R.array.SportSpeice,
				android.R.layout.simple_spinner_item);
		adapterSportSpeice
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinnerSpotMenu.setAdapter(adapterSportSpeice);
		mSpinnerSpotMenu
				.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						mSportSpice = parent.getItemAtPosition(position)
								.toString();
						if (mSportSpice.equals("Run") == true) {
							mCal = 200;
							mSportSpiceflag = true;
						} else if(mSportSpice.equals("Tennis")){
							mCal = 186;
							mSportSpiceflag = true;
						}
						// Toast.makeText(MainActivity.this,""+mCal,Toast.LENGTH_LONG).show();
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});
		Spinner mSpinnerSpotTime = (Spinner) layoyt
				.findViewById(R.id.spinnerSportTime);
		ArrayAdapter adapterSportTime = ArrayAdapter.createFromResource(
				MainActivity.this, R.array.SportTime,
				android.R.layout.simple_spinner_item);
		adapterSportTime
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinnerSpotTime.setAdapter(adapterSportTime);
		mSpinnerSpotTime
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						mSportTime = parent.getItemAtPosition(position)
								.toString();
						if (mSportTime.equals("1") == true) {
							mTime = 1;
							mSportTimeflag = true;
						}else if (mSportTime.equals("2") == true) {
							mTime = 2;
							mSportTimeflag = true;
						}else if (mSportTime.equals("3") == true) {
							mTime = 3;
							mSportTimeflag = true;
						}else if (mSportTime.equals("4") == true) {
							mTime = 4;
							mSportTimeflag = true;
						}else if (mSportTime.equals("5") == true) {
							mTime = 5;
							mSportTimeflag = true;
						}else if (mSportTime.equals("6") == true) {
							mTime = 6;
							mSportTimeflag = true;
						}else if (mSportTime.equals("7") == true) {
							mTime = 7;
							mSportTimeflag = true;
						}else if (mSportTime.equals("8") == true) {
							mTime = 8;
							mSportTimeflag = true;
						}else if (mSportTime.equals("9") == true) {
							mTime = 9;
							mSportTimeflag = true;
						}else if (mSportTime.equals("10") == true) {
							mTime = 10;
							mSportTimeflag = true;
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});
		/*
		 * mButtonOk.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub //Toast.makeText(MainActivity.this,""+mTime*mCal,
		 * Toast.LENGTH_LONG).show(); } });
		 */

		dialog.show();
	}

}
